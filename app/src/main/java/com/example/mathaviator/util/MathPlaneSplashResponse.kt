package com.example.mathaviator.util

import androidx.annotation.Keep

@Keep
data class MathPlaneSplashResponse(val url : String)