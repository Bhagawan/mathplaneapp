package com.example.mathaviator.view

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.content.res.ResourcesCompat
import androidx.core.graphics.drawable.toBitmap
import androidx.core.graphics.scale
import com.example.mathaviator.R
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlin.concurrent.fixedRateTimer
import kotlin.math.*
import kotlin.random.Random

class MathPlane(context: Context, attributeSet: AttributeSet?) : View(context, attributeSet) {
    private var mWidth = 0
    private var mHeight = 0
    private var planeX = 0.0f
    private var planeY = 0.0f
    private var planeAngle = 0.0f

    private var dX = 0.0f
    private var dY = 0.0f
    private var up = false

    private val plane = BitmapFactory.decodeResource(context.resources, R.drawable.plane)
    private val restart = AppCompatResources.getDrawable(context,R.drawable.ic_baseline_replay_24)?.toBitmap()

    private var state = START

    private var question = ""
    private var aFirst = ""
    private var firstPressed = false
    private var aSecond = ""
    private var secondPressed = false
    private var aThird = ""
    private var thirdPressed = false
    private var answer = 0

    companion object {
        const val START = 0
        const val FLYING = 1
        const val FINISH = 2
    }

    init {
        fixedRateTimer("refresh", false, 0, 1000 / 60) {
            CoroutineScope(Dispatchers.Main).launch { invalidate() }
        }
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        if(w > 0) {
            mWidth = w - paddingStart - paddingEnd
            mHeight = h - paddingTop - paddingBottom
            planeY = mHeight / 3.0f
        }
    }

    override fun onDraw(canvas: Canvas?) {
        canvas?.let {
            if(state == FLYING) updatePlane()
            drawPlane(it)
            drawGround(it)
            drawUI(it)
            drawQuestion(it)
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent?): Boolean {
        when(event?.actionMasked) {
            MotionEvent.ACTION_DOWN -> {
                when(state) {
                    START -> {

                    }
                    FLYING -> {
                        checkPress(event.x, event.y)
                    }
                    FINISH -> {

                    }
                }
                return true
            }
            MotionEvent.ACTION_MOVE -> {
                when(state) {
                    START -> {
                    }
                    FLYING -> {
                        checkPress(event.x, event.y)
                    }
                    FINISH -> {
                    }
                }
                return true
            }
            MotionEvent.ACTION_UP -> {
                when(state) {
                    START -> {
                        dX = 1.0f
                        dY = 0.0f
                        state = FLYING
                    }
                    FLYING -> {
                        val p = checkPress(event.x, event.y)
                        if( p > 0) question = ""
                        if(p == answer) up = true
                    }
                    FINISH -> {
                        restart()
                    }
                }
                return true
            }
        }
        return super.onTouchEvent(event)
    }

    private fun drawPlane(c: Canvas) {
        val rotationMatrix = Matrix()
        rotationMatrix.postRotate(planeAngle)
        val rotatedPlane = Bitmap.createBitmap(plane, 0,0,plane.width, plane.height, rotationMatrix, true )
        val p = Paint()
        p.color = Color.WHITE
        c.drawBitmap(rotatedPlane, 0.0f, planeY.coerceAtLeast(mHeight * 0.2f), p)
    }

    private fun drawGround(c : Canvas) {
        if(planeY >= -20) {
            val p = Paint()
            p.color = Color.WHITE
            p.strokeWidth = 3.0f
            val y =(mHeight - planeY - 50.0f).coerceAtLeast(mHeight - 20.0f)
            c.drawLine(0.0f, y, mWidth.toFloat(), y, p)
        }
    }

    private fun drawUI(c: Canvas) {
        val p = Paint()
        p.color = Color.WHITE
        p.strokeWidth = 3.0f
        p.textSize = 60.0f
        p.textAlign = Paint.Align.CENTER

        when(state) {
            START -> {
                p.color = ResourcesCompat.getColor(context.resources, R.color.grey, null)
                val l = (mWidth - 26 * 40) / 2.0f
                val t = (mHeight - 250) / 2.0f
                c.drawRoundRect(l, t, mWidth - l, mHeight - t, 20f, 20f, p)
                p.color = Color.BLACK
                c.drawText("Решайте примеры,", mWidth / 2.0f, mHeight / 2.0f - 60, p)
                c.drawText("чтобы поддерживать самолет", mWidth / 2.0f, mHeight / 2.0f, p)
                c.drawText("в воздухе", mWidth / 2.0f, mHeight / 2.0f + 60, p)
            }
            FLYING -> {
                c.drawText("Дистанция: ${planeX / 10} м", mWidth / 2.0f, mHeight * 0.07f, p)
            }
            FINISH -> {
                p.color = if(thirdPressed) ResourcesCompat.getColor(context.resources, R.color.grey_dark, null) else ResourcesCompat.getColor(context.resources, R.color.grey, null)
                c.drawRoundRect(mWidth * 0.15f, mHeight * 0.3f - 100, mWidth * 0.85f, mHeight * 0.4f, 20f, 20f, p)
                val pad = (restart?.width ?: 90) * 0.5f + 5
                c.drawRoundRect(mWidth * 0.5f - pad, mHeight * 0.7f - pad, mWidth * 0.5f + pad, mHeight * 0.7f + pad, 20f, 20f, p)

                p.color = Color.BLACK
                c.drawText("Самолет пролетел: ${planeX / 10} м", mWidth * 0.5f, mHeight * 0.3f, p)

                p.style = Paint.Style.STROKE
                p.color = Color.WHITE
                c.drawRoundRect(mWidth * 0.5f - pad + 5, mHeight * 0.7f - pad + 5, mWidth * 0.5f + pad - 5, mHeight * 0.7f + pad - 5, 20f, 20f, p)

                restart?.let { c.drawBitmap(restart, (mWidth - restart.width) * 0.5f, mHeight * 0.7f - restart.height * 0.5f, p) }
                question = ""
            }
        }
    }

    private fun updatePlane() {
        dY += 0.005f * if(up) -1 else 1
        if(dY <= -1) up = false

        val newY = planeY + dY * (dY.absoluteValue * 10)

        planeAngle =  Math.toDegrees((sign(dY) * acos(dX / sqrt(dX.pow(2) + dY.pow(2)))).toDouble()).toFloat()
        planeY = newY
        planeX += dX
        if(question.isBlank()) generateQuestion()

        if(planeY >  mHeight - 50 -  plane.height * 0.6f) state = FINISH
    }

    private fun restart() {
        state = START
        planeX = 0.0f
        planeY = mHeight / 3.0f
        planeAngle = 0.0f
    }

    private fun drawQuestion(c: Canvas) {
        if(question.isNotBlank()) {
            val p = Paint()
            p.color = ResourcesCompat.getColor(context.resources, R.color.grey, null)
            p.strokeWidth = 3.0f
            p.textSize = 60.0f
            p.textAlign = Paint.Align.CENTER

            c.drawRoundRect(mWidth * 0.75f - 200, mHeight * 0.5f - 150, mWidth * 0.75f + 200, mHeight * 0.5f - 50, 20f, 20f, p)
            p.color = Color.BLACK
            c.drawText(question, mWidth * 0.75f, mHeight * 0.5f - 80, p)

            p.color = if(firstPressed) ResourcesCompat.getColor(context.resources, R.color.grey_dark, null) else ResourcesCompat.getColor(context.resources, R.color.grey, null)
            c.drawRoundRect(mWidth * 0.75f - 150 - 60, mHeight * 0.5f + 50, mWidth * 0.75f  - 150 + 60, mHeight * 0.5f + 150, 20f, 20f, p)
            p.color = Color.BLACK
            c.drawText(aFirst, mWidth * 0.75f - 150, mHeight * 0.5f + 120, p)

            p.color = if(secondPressed) ResourcesCompat.getColor(context.resources, R.color.grey_dark, null) else ResourcesCompat.getColor(context.resources, R.color.grey, null)
            c.drawRoundRect(mWidth * 0.75f - 60, mHeight * 0.5f + 50, mWidth * 0.75f + 60, mHeight * 0.5f + 150, 20f, 20f, p)
            p.color = Color.BLACK
            c.drawText(aSecond, mWidth * 0.75f, mHeight * 0.5f + 120, p)

            p.color = if(thirdPressed) ResourcesCompat.getColor(context.resources, R.color.grey_dark, null) else ResourcesCompat.getColor(context.resources, R.color.grey, null)
            c.drawRoundRect(mWidth * 0.75f + 150 - 60, mHeight * 0.5f + 50, mWidth * 0.75f  + 150 + 60, mHeight * 0.5f + 150, 20f, 20f, p)
            p.color = Color.BLACK
            c.drawText(aThird, mWidth * 0.75f + 150, mHeight * 0.5f + 120, p)

            p.style = Paint.Style.STROKE
            val pad = 6
            c.drawRoundRect(mWidth * 0.75f - 150 - 60 + pad, mHeight * 0.5f + 50 + pad, mWidth * 0.75f  - 150 + 60 - pad, mHeight * 0.5f + 150 - pad, 20f, 20f, p)
            c.drawRoundRect(mWidth * 0.75f - 60 + pad, mHeight * 0.5f + 50 + pad, mWidth * 0.75f + 60 - pad, mHeight * 0.5f + 150 - pad, 20f, 20f, p)
            c.drawRoundRect(mWidth * 0.75f + 150 - 60 + pad, mHeight * 0.5f + 50 + pad, mWidth * 0.75f  + 150 + 60 - pad, mHeight * 0.5f + 150 - pad, 20f, 20f, p)
        }
    }

    private fun generateQuestion() {
        firstPressed = false
        secondPressed = false
        thirdPressed = false
        val a = Random.nextInt(2,10)
        val b = Random.nextInt(2,10)
        answer = a * b

        question = "$a * $b = "

        aFirst = ((a + 1 * Random.nextInt(-1,2)) * (b + 1 * Random.nextInt(-1,2))).toString()
        aSecond = ((a + 1 * Random.nextInt(-1,2)) * (b + 1 * Random.nextInt(-1,2))).toString()
        aThird = ((a + 1 * Random.nextInt(-1,2)) * (b + 1 * Random.nextInt(-1,2))).toString()
        when(Random.nextInt(3)) {
            0 -> aFirst = (a * b).toString()
            1 -> aSecond = (a * b).toString()
            2 -> aThird = (a * b).toString()
        }
    }

    private fun checkPress(x: Float, y: Float) : Int{
        firstPressed = false
        secondPressed = false
        thirdPressed = false
        if(y in (mHeight * 0.5f + 50)..(mHeight * 0.5f + 150)) {
            when(x) {
                in (mWidth * 0.75f - 150 - 60)..(mWidth * 0.75f  - 150 + 60) -> {
                    firstPressed = true
                    return aFirst.toInt()
                }
                in (mWidth * 0.75f - 60)..(mWidth * 0.75f + 60) -> {
                    secondPressed = true
                    return aSecond.toInt()
                }
                in (mWidth * 0.75f + 150 - 60)..(mWidth * 0.75f  + 150 + 60) -> {
                    thirdPressed = true
                    return aThird.toInt()
                }
            }
        }
        return -1
    }


}